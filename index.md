---
layout: default
title: About
---

# {{ site.author }} 

{% marginnote "affiliations" "University of Oregon<br />PhD Student, Department of Psychology<br /><small><a href='http://psi.uoregon.edu/'>Prevention Science Institute</a><br /><a href='http://ctn.uoregon.edu/'>Center for Translational Neuroscience</a><br /><a href='http://dsn.uoregon.edu/'>Developmental social neuroscience lab</a><br /><a href='http://psdlab.uoregon.edu/'>Personality and social dynamics lab</a><br /><a href='http://blogs.uoregon.edu/rclub/'>R Club</a></small>"  %}

Broadly, my work focuses on the **development** of **individual differences** in **cognition** and **motivation**, and their consequences for  **health and well-being**. Specifically, I focus on decision making under uncertainty, and social motivations during adolescence, as well as lifespan development of personality. A modeler at heart, I use sophisticated computational and statistical tools to understand the processes and structures underlying observed behavior in response to games and questionnaires. These tools include multi-level modeling, Bayesian modeling, structural equation modeling, machine learning, and network analysis. In many cases, I also relate behavior to physiological response (e.g., fMRI BOLD response).
	
