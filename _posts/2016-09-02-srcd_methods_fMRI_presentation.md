---
layout: post
title: Using fMRI to study child and adolescent development
date: 2014-09-11
category: Presentations
tags: fMRI, methods, overview, srcd
---

Every other year, The Society for Research in Child Development hosts a series of smaller, special topic conferences. This year I was lucky to go to the [Developmental Methodology](http://www.srcd.org/meetings/special-topic-meetings/2-developmental-methodology) conference. I was blown away by what I saw there, and left very inspired.

My contribution was to present the DSN lab's guide to best practices both for our fellow imagers, as well as for those reading the literature without much background. Here it is:

>Flournoy, J. C., Moore III, W. E., & Pfeifer, J. H. (2014, September 11). [Using fMRI to study child and adolescent development: Thoughts from the trenches](http://pages.uoregon.edu/flournoy/thoughts_from_the_trenches/#/). Presented at Developmental Methodology, San Diego, CA.
